module.exports = function(wallaby) {
  process.env.NODE_ENV = "development";
  return {
    debug: true,
    env: {
      type: "node",
    },
    files: [
      "!src/**/*-spec.ts",
      "src/**/*.ts",
    ],
    reportConsoleErrorAsError: true,
    testFramework: "mocha",
    tests: [
//      "src/**/*.intg-spec.ts",
      "src/**/*.unit-spec.ts",
    ],
  };
};
