import * as S3 from "aws-sdk/clients/s3";
import * as URL from "url";
import {
  IS3NukeRequest,
} from "./s3nuke";

export function removeObjects(params: IS3NukeRequest): Promise<S3.Types.DeleteObjectsOutput> {

  const requests: Array<Promise<S3.Types.DeleteObjectsOutput>> = [];
  let isMore;

  do {
    listObjectsSync(params)
      .then((objectList: S3.Types.ListObjectsV2Output) => {
        isMore = objectList.ContinuationToken = objectList.IsTruncated ? objectList.NextContinuationToken : null;
        requests.push(params.Client.deleteObjects(listObjectsV2Output2deleteObjectsRequest(objectList)).promise());
      })
      .catch((err: Error) => { Promise.reject(err); });
  } while (isMore);

  return Promise.all(requests);

  // return Promise.all(requests)
  //   .then((res) => {
  //     let batchResponse = {
  //       Count: 0,
  //       Deleted: [],
  //       Errors: [],
  //     };

  //     res.forEach((batch) => {
  //       console.log(batchResponse.Count);
  //       batchResponse.Count += batch.Deleted.length;
  //       batchResponse.Deleted.push(...batch.Deleted);
  //       batchResponse.Errors.push(...batch.Errors);
  //     });

  //     return batchResponse;
  //   });
}

export async function listObjectsSync(params: IS3NukeRequest): Promise<S3.Types.ListObjectsV2Output> {
  try {
    return await params.Client.listObjectsV2(s3nukeRequest2listObjectsRequest(params)).promise();
  } catch (err) {
    return Promise.reject(err);
  }
}

export async function removeObjectsSync(params: IS3NukeRequest): Promise<S3.Types.DeleteObjectsOutput> {
  try {
    return await removeObjects(params);
  } catch (err) {
    return Promise.reject(err);
  }
}

export function removeBucket(params: IS3NukeRequest): Promise<any> {
  return params.Client.deleteBucket(params).promise()
    .then((res: any) => { return Promise.resolve(res); })
    .catch((err: Error) => { return Promise.reject(err); });
}

export function parseS3URL(url: string): any {
  if (!url.startsWith("s3://")) {
    throw new Error("UNSUPPORTED PROTOCOL");
  }
  const parsed = URL.parse(url.replace(/^\/|\/$/g, ""), false, true);
  return [parsed.hostname, parsed.path.replace(/^\/|\/$/g, "")];
}

export function listObjectsV2Output2deleteObjectsRequest
(input: S3.Types.ListObjectsV2Output): S3.Types.DeleteObjectsRequest {
  return {
    Bucket: input.Name,
    Delete: { Objects: input.Contents.map((o: S3.Types.ObjectIdentifier) => { return { Key: o.Key }; }) },
  } as S3.Types.DeleteObjectsRequest;
}

export function s3nukeRequest2listObjectsRequest(input: IS3NukeRequest): S3.Types.ListObjectsV2Request {
  return {
    Bucket: input.Bucket,
    MaxKeys: input.BatchSize,
    Prefix: input.Prefix ? input.Prefix : null,
  };
}
