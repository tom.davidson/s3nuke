import * as chai from "chai";
import {
  listObjectsV2Output2deleteObjectsRequest,
  parseS3URL,
} from "./utils";

const expect = chai.expect;

describe("parseS3URL", () => {
  it("should reject non-s3 urls", () => {
    const url = "http:/domain.com/path/too";
    expect(parseS3URL.bind(parseS3URL, url)).to.throw("UNSUPPORTED PROTOCOL");
  });
  it("should parse bucket name", () => {
    const url = "s3://my-s3-bucket-name/prefix/";
    expect(parseS3URL(url)[0]).to.equal("my-s3-bucket-name");
  });
  it("should parse prefix", () => {
    let url = "s3://my-s3-bucket-name/foo/bar/";
    expect(parseS3URL(url)[1]).to.equal("foo/bar");
    url = "s3://my-s3-bucket-name/*";
    expect(parseS3URL(url)[1]).to.equal("*");
    url = "s3://my-s3-bucket-name/foo/ba";
    expect(parseS3URL(url)[1]).to.equal("foo/ba");
  });
});
