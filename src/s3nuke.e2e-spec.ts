import * as S3 from "aws-sdk/clients/s3";
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as s3g from "s3objectgenerator";
import {
  IS3NukeRequest,
  S3Nuke,
} from "./s3nuke";

chai.use(chaiAsPromised);
const expect = chai.expect;

describe("nuke:", () => {
  const s3 = new S3({
    apiVersion: "2006-03-01",
  });

  const testBucketName = "S3ObjectGenerator-TestBucket";

  // before(function (done) {
  //   const bucketParams = {
  //     Bucket: testBucketName,
  //   };
  //   s3.createBucket(bucketParams, function (err, data) {
  //     if (err) {
  //       done(err);
  //     } else {
  //       console.log("S3 Bucket is setup: ", data.Location + "\n");
  //       done();
  //     }
  //   });
  // });

  //   it("should nuke a non-empty bucket", () => {
  //     let generateParams = <s3g.IS3ObjectGenerateRequest> {
  //       Bucket: testBucketName,
  //       Client: s3,
  //       Quantity: 10,
  //     }

  //     let nukeParams = <IS3NukeRequest> {
  //       BatchSize: 100,
  //       Bucket: testBucketName,
  //       Client: s3,
  //     }

  //    // s3g.generate(generateParams).then(S3Nuke(nukeParams));

  //    S3Nuke(nukeParams);

  //    //.then((res) => { console.log("no error")})
  //    //.catch((res: any) => {console.log(res)});

  //   });

});
