import * as S3 from "aws-sdk/clients/s3";
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as s3g from "s3objectgenerator";
import {
  IS3NukeRequest,
} from "./s3nuke";
import {
  removeBucket,
  removeObjects,
  removeObjectsSync,
} from "./utils";

chai.use(chaiAsPromised);
const expect = chai.expect;
const s3 = new S3({
  apiVersion: "2006-03-01",
});

// describe("removeBucket", () => {
//   const testBucketName = "S3Nuke-TestBucket-removeBucket";

//   beforeEach(() => {
//     console.log("    Setup - create S3 Bucket");
//     return s3.createBucket({ Bucket: testBucketName }).promise()
//       .then((data) => { return Promise.resolve(); })
//       .catch((err) => { return Promise.reject(err); });
//   });

  // after(() => {
  //   console.log("    Teardown - destroy S3 Bucket");
  //   return s3.deleteBucket({ Bucket: testBucketName }).promise()
  //     .then((data) => { return Promise.resolve(); })
  //     .catch((err) => {
  //       if (err.code === "NoSuchBucket") {
  //         return Promise.resolve();
  //       }
  //       return Promise.reject(err);
  //     });
  // });

//   let params = <IS3NukeRequest> {
//     Bucket: testBucketName,
//   };

//   it("should destroy the bucket", () => {
//     return expect(removeBucket(params, s3)).eventually.to.be.fulfilled;
//   });

// });

// mocha needs 'this' to set timeout
// tslint:disable-next-line:only-arrow-functions
describe("removeObjects", function() {
  this.timeout(5000);

  const testBucketName = "S3Nuke-TestBucket-removeObjects";

  before(() => {
    // tslint:disable-next-line:no-console
    console.log("    Setup - create S3 Bucket");
    return s3.createBucket({ Bucket: testBucketName }).promise()
      .then((data) => { return Promise.resolve(); })
      .catch((err) => { return Promise.reject(err); });
  });

  // after(() => {
  //   // tslint:disable-next-line:no-console
  //   console.log("    Teardown - destroy S3 Bucket");
  //   return s3.deleteBucket({ Bucket: testBucketName }).promise()
  //     .then((data) => { return Promise.resolve(); })
  //     .catch((err) => { return Promise.reject(err); });
  // });

  const params: IS3NukeRequest = {
    BatchSize: 15,
    Bucket: testBucketName,
    Client: s3,
  };

  const generateParams: s3g.IS3ObjectGenerateRequest = {
    Bucket: testBucketName,
    Client: s3,
    Quantity: 50,
  };

  it("should empty the bucket", () => {
    s3g.generateSync(generateParams);

    return removeObjectsSync(params)
      .then((res: any) => {
        // console.log("TEST RESULTS:"); console.log(res);
        return res;
      })
      .catch((err: Error) => {
        // console.error(err);
        return Promise.reject(err);
      });
  });

});
