import * as S3 from "aws-sdk/clients/s3";
import {
  parseS3URL,
  removeBucket,
  removeObjects,
  s3nukeRequest2listObjectsRequest,
} from "./utils";

const S3NukeDefaultConfig: IS3NukeRequest = {
  BatchSize: 500,
  Bucket: null,
  Client: null,
  Prefix: null,
};

export interface IS3NukeRequest {
  // The max number of Objects to remove with each cycle. Default is 1000.
  BatchSize?: number;

  // Name of the bucket to list.
  Bucket: S3.Types.BucketName;

  // The S3 client to use.
  Client: S3;

  // Filter the targeted objects with Amazon S3 Prefix.
  Prefix?: S3.Types.Prefix;
}

export function is3NukeRequest(object: any): object is IS3NukeRequest {
  return "Bucket" in object && ("Client" in object || "BatchSize" in object || "Prefix" in object);
}

export function S3Nuke(target: IS3NukeRequest | string)
// tslint:disable-next-line:typedef-whitespace
  : Promise<S3.Types.Delete> | Promise<S3.Types.DeleteObjectsOutput> {

  let S3NukeRequest: IS3NukeRequest;

  if (is3NukeRequest(target)) {
    S3NukeRequest = Object.assign(S3NukeDefaultConfig, target);
    S3NukeRequest.Client = target.Client ? target.Client : new S3({ apiVersion: "2006-03-01" });
  } else if (typeof target === "string") {
    S3NukeRequest = Object.assign(S3NukeDefaultConfig);
    [S3NukeRequest.Bucket, S3NukeRequest.Prefix] = parseS3URL(target);
    S3NukeRequest.Client = new S3({ apiVersion: "2006-03-01" });
  } else {
    throw new Error("UNSUPPORTED PARAMS");
  }

  const params = S3NukeRequest;

  return removeObjects(params)
    .then((res: S3.Types.DeleteBucketRequest) => {
      if (!params.Prefix) {
        return removeBucket(params);
      } else {
        return Promise.resolve(res);
      }
    });
};
